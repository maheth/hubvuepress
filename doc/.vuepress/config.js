module.exports = {
    title: 'PeopleSoft Hub',
    description: 'PeopleSoft Interaction Hub | Product Overview',
    themeConfig: {
        nav: [
          { text: 'Home', link: '/' },
          
        
        ],
        sidebar: [
            '/documentation',
            
          ],
          lastUpdated: 'Last Updated', // string | boolean
      }
  }