# Documentation

## Overview

The PeopleSoft Interaction Hub is a portal which helps in simplifying the organization of content creation processes.

The content creation process starts with: 

1. Creating and Organizing the content 

2. Managing approvals

3. Managing multiple user access

4. Managing the publication process

## Understanding the Set Up of Content Management System
 
The steps required to implement content management are:

1. Identify how to manage and separate responsibilities
   
      - Responsibilities are separated by tasks such as adding folders and publishing content, which are assigned to privilege sets on the Privilege Sets page
      - These privilege sets consist of actions that can be performed on the folder or the content which is located in that folder, when creating the folder these privilege set actions are associated with member roles and user ID's

      > Refer to [Summary of Delivered Security Data](https://docs.oracle.com/cd/F18665_01/ps91pbr13/eng/ps/psad/task_SummaryofDeliveredSecurityData-527273.html)


2. Identify where the content can be stored
      - The content is stored on File Transfer Protocol(FTP) servers, the choice of content links is based on the information from the URL Maintenance page


      > Refer to [File Storage Page](https://docs.oracle.com/cd/F18665_01/ps91pbr13/eng/ps/pscm/task_AdministeringtheContentManagementSystem-527a9b.html##ue7a57358-18d0-45e6-9b38-d3fedf2d5ecd)
  
3. Identify where images can be stored
      - Using an FTP service the images can be uploaded, accessed and rendered

      > **Tip**: The FTP extension should be installed on a single web server



## Understanding the Folder Hierarchy

The content management system uses folders and subfolders to organize content. Each feature of the system has a a folder and subfolder Hierarchy.

Folders at the root of Hierarchy are known as top folders and can contain subfolders within subfolders. The content can be created in any folder except the top folder.

The functions and menu items which are used to access the content are:

- Browse Folders
- Search Folders
- Search Content
- Maintain Content

![Content Hierarchy](/Hierarchy.png)

The folder Hierarchy in the News Publications feature are a little different. The top folders are called as publications, and only one level subfolders are allowed which are called sections. The content within these sections are called articles.

![News Publications Hierarchy](/publications.png)


## Understanding Managed Content

The Managed Content feature of the content management system allows:
- Creating the content
- Managing the content in folders
- Checking the content in and out
- Creating new versions of the content
- Making approved content to be used with other features such as News Publications, collaborative workspaces and more

Depending on the content flow the system sets a status to the content based on where it is in process flow

![Content Process Flow](/contentprocess.png)

### Content Check In and Out

While creating a new content the system automatically locks and checks out the content. Checking in the content unlocks the content for editing. When the content is checked out it produces a new version which is assigned as Draft status by the system.
 
### Cancel Check Out
Based on the status of the last version of content that is checked out, cancelling the check out brings about these changes based on the conditions:

- Cancelling the check out unlocks the content, if only there is one version.
- Cancelling the check out unlocks the content and assigns a draft status, if the check out content is submitted for approval
- Cancelling the check out deletes the previous version, if the check out content is at draft status and there is more than one version

### Submitting for Approval
Once the content is submitted for approval the system provides it a status of Pending Approval
, The approver can has an option of approving the content or return it back for rework.

When the content does get approved the system assigns a status of Approved, which means that the content is ready to be published.


## Understanding Categorized Content
The Categorized Content feature of the content management system allows to group data rising from many sources. Content from file servers and web servers are grouped together and placed in categorized content Hierarchy.

Access control is governed by User ID, or role based on which the content can be edited, viewed and published.

They are two ways to create categorized content which is automatic and manual.

The manual process have these steps:

1. Folder administrators create top folders and subfolders in the categorized content 
2. Folder administrators then assign different security roles to members of the specified folders
3. The authors create and save content in these folders
4. The authors also submit the content for approval so it can be reviewed
5. The publishers review the content which is submitted and approve to publish it
6. The viewers browse by category to view the published content

![Process for creating and publishing the categorized content](/catergorizedcontent.png)

## News Publications
Organizations can use news publications to group content by sections and articles and publish them to a target audience via a homepage pagelet

To meet various communications needs of the organization they are many news publications such as company-wide news, local news, and department news.

The process flow for creating a news publication is 

1. The top administrator creates a news publication and provides audience as a viewer security role
2. The folder administrator creates sections in news publication
3. The contributors create the content for news articles and add it to the sections
4. The contributors also submit articles for publication
5. The publishers review the articles for approval or rejection
6. After approval the publishers publish the articles
7. The contributors and publishers choose the top stories 
8. The top administrator publish the articles as pagelets
9. If the viewers see the submit button they can articles through the pagelet

![News Articles Publication Process](/newspublication.png)

## Web Magazine
The online publishing feature of Web Magazine allows creation and publication of content such as newsletters, policy handbooks in a graphical format. This helps in reducing printing cost since the web based content can be easily updated.

The benefits of using the feature of Web Magazine are:

- The content is managed separately from the page presentation layout which helps in content being easily updated and reused
- Content components such as images are stored in a repository which is separate from content management repository
- Presentation templates can be used and customized for any type of web page
  
  